//
//  ServerRequest.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class ServerRequest {
    public init(){
    
    }
    func getMostViewedArticles(period:Int,completion:((_ success:Bool,_ articles:[Article]?)->Void)?){
        SundeedAPIRequest<Article>().withURL(APISettings.getNewYorkTimesURL(forPeriod: period)).withMethod("GET").requestArray { (response) in
            completion?(response.status == "OK",response.results)
        }
    }
}
