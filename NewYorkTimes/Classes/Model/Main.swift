//
//  MainController.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class Main{
    public static let controller:Main = Main()
    private let server: ServerRequest = ServerRequest()
    private var imageCache:[String:UIImage] = [:]
    public var user:User = User()
    public init(){
        
    }
    
    func getMostViewedArticles(period:Int,completion:((_ success:Bool,_ articles:[Article]?)->Void)?){
        server.getMostViewedArticles(period: period) { [weak self] (success, articles) in
            if success && articles != nil{
                self?.user.articles = articles!
            }
            completion?(success,articles)
        }
        
    }
    
    func getImage(url:String?,completion:((_ image:UIImage?)->Void)?){
        DispatchQueue.global().async {
            if url != nil {
                if self.imageCache.contains(where: { (value) -> Bool in
                    return value.key == url
                }){
                    DispatchQueue.main.async {
                        completion?(self.imageCache[url!])
                    }
                }
                else{
                    if let url = URL(string: url!) {
                        do {
                            let image = try UIImage(data: Data(contentsOf: url))
                            DispatchQueue.main.async {
                                completion?(image)
                            }
                        }
                        catch{
                            DispatchQueue.main.async {
                                completion?(UIImage(named:"article"))
                            }
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            completion?(UIImage(named:"article"))
                        }
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    completion?(UIImage(named:"article"))
                }
                
            }
        }
    }
}
