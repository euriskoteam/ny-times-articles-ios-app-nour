//
//  Period.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/24/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

enum Period:Int{
    case day = 1
    case week = 7
    case month = 30
}
