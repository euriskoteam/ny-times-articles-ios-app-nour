//
//  _Array.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/24/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

extension Array where Element:Article{
    func containsArticle(_ article:Article)->Bool{
        return self.contains(where: { (a) -> Bool in
            return a.id == article.id
        })
    }
    
    func getArticle(_ article:Article)->Article?{
        return self.first(where: { (a) -> Bool in
            return a.id == article.id
        })
    }
}
