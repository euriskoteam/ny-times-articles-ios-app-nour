//
//  Media.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

class Media:SundeedAPIMappable{
    
    var type:String?
    var subtype:String?
    var caption:String?
    var copyright:String?
    var approvedForSyndication:Bool?
    var mediaMetadata:[MetaData]?
    
    required init(){
        
    }
    func sundeedAPIMapping(map: SundeedAPIMap) {
        self.type <- map["type"]
        self.subtype <- map["subtype"]
        self.caption <- map["caption"]
        self.copyright <- map["copyright"]
        self.approvedForSyndication <- (map["approved_for_syndication"],APIBoolConverter())
        self.mediaMetadata <- map["media-metadata"]
    }
}
