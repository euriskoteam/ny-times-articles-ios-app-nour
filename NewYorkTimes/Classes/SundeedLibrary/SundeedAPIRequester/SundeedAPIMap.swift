//
//  SundeedAPIMap.swift
//  AppointSync
//
//  Created by Nour Sandid on 1/8/19.
//  Copyright © 2019 SUNDEED. All rights reserved.
//

import Foundation

public class SundeedAPIMap {
    private var map:[String:Any] = [:]
    fileprivate var key:String?
    var currentValue:Any?
    
    subscript(key:String)->SundeedAPIMap{
        self.key = key
        if map.contains(where: { (key1, value1) -> Bool in
            return key1 == key
        }){
            self.currentValue = map[key]
        }
        return self
    }
    init(dictionnary:[String: Any]) {
        self.map = dictionnary
    }
}

infix operator <-
public func <- <T>(left: inout T, right: (SundeedAPIMap,SundeedAPIConverter)) {
    left = right.1.fromJSON(value: right.0.currentValue as Any) as! T
}
public func <- <T:SundeedAPIMappable>(left: inout [T], right: SundeedAPIMap) {
    if let values = right.currentValue as? NSArray{
        var array:[T] = []
        for value in values {
            if value is [String:Any] {
                let object = T()
                let map = SundeedAPIMap(dictionnary: value as! [String:Any])
                object.sundeedAPIMapping(map: map)
                array.append(object)
            }
        }
        left = array
    }
}
public func <- <T:SundeedAPIMappable>(left: inout [T?], right: SundeedAPIMap) {
    if let values = right.currentValue as? NSArray{
        var array:[T] = []
        for value in values {
            if value is [String:Any] {
                let object = T()
                let map = SundeedAPIMap(dictionnary: value as! [String:Any])
                object.sundeedAPIMapping(map: map)
                array.append(object)
            }
        }
        left = array
    }
}
public func <- <T:SundeedAPIMappable>(left: inout [T?]?, right: SundeedAPIMap) {
    if let values = right.currentValue as? NSArray{
        var array:[T] = []
        for value in values {
            if value is [String:Any] {
                let object = T()
                let map = SundeedAPIMap(dictionnary: value as! [String:Any])
                object.sundeedAPIMapping(map: map)
                array.append(object)
            }
        }
        left = array
    }
}
public func <- <T:SundeedAPIMappable>(left: inout [T]?, right: SundeedAPIMap) {
    if let values = right.currentValue as? NSArray{
        var array:[T] = []
        for value in values {
            if value is [String:Any] {
                let object = T()
                let map = SundeedAPIMap(dictionnary: value as! [String:Any])
                object.sundeedAPIMapping(map: map)
                array.append(object)
            }
        }
        left = array
    }
}
public func <- <T:SundeedAPIMappable>(left: inout T, right: SundeedAPIMap){
    if let value = right.currentValue as? [String:Any] {
        let object = T()
        let map = SundeedAPIMap(dictionnary: value)
        object.sundeedAPIMapping(map: map)
        left = object
    }
}
public func <- <T:SundeedAPIMappable>(left: inout T?, right: SundeedAPIMap){
    if let value = right.currentValue as? [String:Any] {
        let object = T()
        let map = SundeedAPIMap(dictionnary: value)
        object.sundeedAPIMapping(map: map)
        left = object
    }
}
public func <- (left: inout String?, right: SundeedAPIMap){
    if let value = right.currentValue as? String {
        left = value
    }
}
public func <-(left: inout [String]?, right: SundeedAPIMap){
    var array:[String] = []
    if let values = right.currentValue as? [String] {
        for value in values {
            array.append(value)
        }
        left = array
    }
}
public func <-(left: inout Int?, right: SundeedAPIMap){
    if let value = right.currentValue as? Int {
        left = value
    }
}
public func <-(left: inout [Int]?, right: SundeedAPIMap){
    var array:[Int] = []
    if let values = right.currentValue as? [Int] {
        for value in values {
            array.append(value)
        }
        left = array
    }
}
public func <-(left: inout Double?, right: SundeedAPIMap){
    if let value = right.currentValue as? Double {
        left = value
    }
}
public func <-(left: inout [Double]?, right: SundeedAPIMap){
    var array:[Double] = []
    if let values = right.currentValue as? [Double] {
        for value in values {
            array.append(value)
        }
        left = array
    }
}
public func <-(left: inout Bool?, right: SundeedAPIMap){
    if let value = right.currentValue as? Bool {
        left = value
    }
}
public func <-(left: inout [Bool]?, right: SundeedAPIMap){
    var array:[Bool] = []
    if let values = right.currentValue as? [Bool] {
        for value in values {
            array.append(value)
        }
        left = array
    }
}
public func <-(left: inout Float?, right: SundeedAPIMap){
    if let value = right.currentValue as? Float {
        left = value
    }
}
public func <-(left: inout [Float]?, right: SundeedAPIMap){
    var array:[Float] = []
    if let values = right.currentValue as? [Float] {
        for value in values {
            array.append(value)
        }
        left = array
    }
}


