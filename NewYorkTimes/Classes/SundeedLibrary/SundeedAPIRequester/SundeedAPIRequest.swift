//
//  SundeedAPIRequest.swift
//  AppointSync
//
//  Created by Nour Sandid on 1/8/19.
//  Copyright © 2019 Sundeed. All rights reserved.
//

import UIKit

class SundeedAPIRequest<T:SundeedAPIMappable> {
    private var url:String!
    private var method:String = "GET"
    private var parameters:[String:Any] = [:]
    private var headers:[String:String] = [:]
    private var image:UIImage?
    private var document:UIDocument?
    
    func withURL(_ url:String) -> SundeedAPIRequest{
        self.url = url
        return self
    }
    
    func withMethod(_ method:String) -> SundeedAPIRequest{
        self.method = method
        return self
    }
    
    func withParameters(_ parameters:[String:Any]) ->SundeedAPIRequest{
        self.parameters = parameters
        return self
    }
    func withHeaders(_ headers:[String:String])->SundeedAPIRequest{
        self.headers = headers
        return self
    }
    func withImage(_ image:UIImage)->SundeedAPIRequest{
        self.image = image
        return self
    }
    func withDocument(_ document:UIDocument)->SundeedAPIRequest{
        self.document = document
        return self
    }
    private func buildRequest(_ url:URL)->URLRequest{
        var request = URLRequest(url: url)
        request.httpMethod = self.method
        if self.method == "POST"{
            let body = try? JSONSerialization.data(withJSONObject: self.parameters)
            request.httpBody = body
        }
        
        request.allHTTPHeaderFields = self.headers
        
        
        
        URLSession.shared.configuration.allowsCellularAccess = true
        URLSession.shared.configuration.shouldUseExtendedBackgroundIdleMode = true
        URLSession.shared.configuration.waitsForConnectivity = true
        return request
    }
    func requestObject(completion: ((_ response:Response<T>)->Void)?){
        DispatchQueue.global().async {
            guard self.url != nil else{
                print("Please specify the URL")
                return
            }
            guard let url = URL(string: self.url) else{
                print("Please Enter a valid URL")
                return
            }
            
            let request = self.buildRequest(url)
            let task = URLSession.shared.dataTask(with: request) { (data, resp, error) in
                guard let data = data else {
                    let response = Response<T>()
                    response.status = "ERROR"
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                    return
                    
                }
                if let jsonDictionnary = self.convertToDictionary(data: data) {
                    let response = Response<T>()
                    let map = SundeedAPIMap(dictionnary:jsonDictionnary)
                    response.sundeedAPIMapping(map: map)
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                }
                else{
                    let response = Response<T>()
                    response.status = "ERROR"
                    completion?(response)
                }
                
            }
            task.resume()
        }
        
    }
    func requestArray(completion: ((_ response:ArrayResponse<T>)->Void)?){
        DispatchQueue.global().async {
            guard self.url != nil else{
                print("Please specify the URL")
                return
            }
            guard let url = URL(string: self.url) else{
                print("Please Enter a valid URL")
                return
            }
            let request = self.buildRequest(url)
            let task = URLSession.shared.dataTask(with: request) { (data, resp, error) in
                guard let data = data else {
                    let response = ArrayResponse<T>()
                    response.status = "ERROR"
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                    return
                    
                }
                if let jsonDictionnary = self.convertToDictionary(data: data) {
                    let response = ArrayResponse<T>()
                    let map = SundeedAPIMap(dictionnary:jsonDictionnary)
                    response.sundeedAPIMapping(map: map)
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                }
                else{
                    let response = ArrayResponse<T>()
                    response.status = "ERROR"
                    completion?(response)
                }
                
            }
            task.resume()
        }
    }
    func uploadImage(completion: ((_ response:Response<T>)->Void)?){
        DispatchQueue.global().async {
            guard self.url != nil else{
                print("Please specify the URL")
                return
            }
            guard let url = URL(string: self.url) else{
                print("Please Enter a valid URL")
                return
            }
            guard self.image != nil else{
                print("No Image to upload")
                return
            }
            URLSession.shared.configuration.allowsCellularAccess = true
            URLSession.shared.configuration.shouldUseExtendedBackgroundIdleMode = true
            URLSession.shared.configuration.waitsForConnectivity = true
            
            let boundary: String = "------VohpleBoundary4QuqLuM1cE5lMwCy"
            let contentType: String = "multipart/form-data; boundary=\(boundary)"
            self.headers["Content-Type"] = contentType
            let request = NSMutableURLRequest()
            request.url = url
            request.allHTTPHeaderFields = self.headers
            request.httpShouldHandleCookies = false
            request.timeoutInterval = 60
            request.httpMethod = "POST"
            
            let body = NSMutableData()
            for (key, value) in self.parameters {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
            let fileName: String = "file"
            if self.image != nil {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(fileName)\"; filename=\"image.png\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:image/jpeg\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(self.image!.jpegData(compressionQuality: 0.2)!)
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            request.httpBody = body as Data
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest) { (data, resp, error) in
                if(error != nil){
                    let response = Response<T>()
                    response.status = "ERROR"
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                }
                guard let data = data else { return }
                let response = Response<T>()
                if let dic = self.convertToDictionary(data: data) {
                    let map = SundeedAPIMap(dictionnary:dic)
                    response.sundeedAPIMapping(map: map)
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                }
                else{
                    let response = Response<T>()
                    response.status = "ERROR"
                    DispatchQueue.main.async {
                        completion?(response)
                    }
                }
            }
            task.resume()
        }
        
    }
    
    func uploadDocument(completion: ((_ response:Response<T>)->Void)?){
        DispatchQueue.global().async {
            guard self.url != nil else{
                print("Please specify the URL")
                return
            }
            guard let url = URL(string: self.url) else{
                print("Please Enter a valid URL")
                return
            }
            guard self.document != nil else{
                print("No Document to upload")
                return
            }
            URLSession.shared.configuration.allowsCellularAccess = true
            URLSession.shared.configuration.shouldUseExtendedBackgroundIdleMode = true
            URLSession.shared.configuration.waitsForConnectivity = true
            
            let boundary: String = "------VohpleBoundary4QuqLuM1cE5lMwCy"
            let contentType: String = "multipart/form-data; boundary=\(boundary)"
            self.headers["Content-Type"] = contentType
            let request = NSMutableURLRequest()
            request.url = url
            request.allHTTPHeaderFields = self.headers
            request.httpShouldHandleCookies = false
            request.timeoutInterval = 60
            request.httpMethod = "POST"
            
            let body = NSMutableData()
            for (key, value) in self.parameters {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append("\(value)\r\n".data(using: String.Encoding.utf8)!)
            }
            let fileExtension = self.document!.fileURL.absoluteString.split(separator: ".").last!
            let fileName: String = "file"
            if self.document != nil {
                body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Disposition: form-data; name=\"\(fileName)\"; filename=\"image.\(fileExtension)\"\r\n".data(using: String.Encoding.utf8)!)
                body.append("Content-Type:application/octet-stream\r\n\r\n".data(using: String.Encoding.utf8)!)
                body.append(try! Data(contentsOf: self.document!.fileURL))
                body.append("\r\n".data(using: String.Encoding.utf8)!)
            }
            body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
            request.httpBody = body as Data
            let session = URLSession.shared
            let task = session.dataTask(with: request as URLRequest) { (data, resp, error) in
                if(error != nil){
                    print(String(data: data!, encoding: .utf8) ?? "No response from server")
                }
                guard let data = data else { return }
                let response = Response<T>()
                let map = SundeedAPIMap(dictionnary:self.convertToDictionary(data: data)!)
                response.sundeedAPIMapping(map: map)
                DispatchQueue.main.async {
                    completion?(response)
                }
            }
            task.resume()
        }
        
    }
    
    func convertToDictionary(data: Data) -> [String: Any]? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
}


