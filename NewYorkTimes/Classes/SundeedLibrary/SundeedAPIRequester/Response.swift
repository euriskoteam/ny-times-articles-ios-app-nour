//
//  Response.swift
//  AppointSync
//
//  Created by Nour Sandid on 1/8/19.
//  Copyright © 2019 SUNDEED. All rights reserved.
//

import Foundation

class Response<T:SundeedAPIMappable>:SundeedAPIMappable{
    var status:String!
    var copyright:String!
    var num_results:Int!
    var results:T!
    
    required init(){
        
    }
    
    func sundeedAPIMapping(map: SundeedAPIMap) {
        self.status <- map["status"]
        self.copyright <- map["copyright"]
        self.num_results <- map["num_results"]
        self.results <- map["results"]
    }
}
class ArrayResponse<T:SundeedAPIMappable>:SundeedAPIMappable{
    var status:String!
    var copyright:String!
    var num_results:Int!
    var results:[T]!
    
    required init(){
        
    }
    
    func sundeedAPIMapping(map: SundeedAPIMap) {
        self.status <- map["status"]
        self.copyright <- map["copyright"]
        self.num_results <- map["num_results"]
        self.results <- map["results"]
    }
}

