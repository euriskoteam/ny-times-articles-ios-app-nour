//
//  SundeedAPIConverter.swift
//  AppointSync
//
//  Created by Nour Sandid on 1/8/19.
//  Copyright © 2019 SUNDEED. All rights reserved.
//

import Foundation

public protocol SundeedAPIConverter:class{
    func fromJSON(value:Any)->Any?
}

