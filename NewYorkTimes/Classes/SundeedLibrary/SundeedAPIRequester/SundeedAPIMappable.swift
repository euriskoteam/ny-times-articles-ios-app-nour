//
//  SundeedAPIMappable.swift
//  AppointSync
//
//  Created by Nour Sandid on 1/8/19.
//  Copyright © 2019 SUNDEED. All rights reserved.
//

import UIKit

public protocol SundeedAPIMappable:class {
    func sundeedAPIMapping(map:SundeedAPIMap)
    init()
    func initialize()->SundeedAPIMappable
}
extension SundeedAPIMappable{
    func initialize()->SundeedAPIMappable{
        return type(of: self).init()
    }
}
