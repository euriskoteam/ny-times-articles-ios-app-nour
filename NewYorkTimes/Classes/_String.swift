//
//  _String.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/24/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

extension String {
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        return NSLocalizedString(self, tableName: tableName, value: self, comment: "")
    }
}
