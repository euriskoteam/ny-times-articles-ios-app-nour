//
//  Converters.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

class APIDateConverter:SundeedAPIConverter{
    func fromJSON(value: Any) -> Any? {
        guard let dateString = value as? String else{
            return nil
        }
        return Date.fromYMDDate(dateString: dateString)
    }
}

class APIBoolConverter:SundeedAPIConverter{
    func fromJSON(value: Any) -> Any? {
        guard let boolInt = value as? Int else{
            return nil
        }
        return boolInt != 0
    }
}
