//
//  Article.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation


class Article:SundeedAPIMappable{
    
    var url:String?
    var adxKeywords:String?
    var column:String?
    var section:String?
    var byline:String?
    var type:String?
    var title:String?
    var abstract:String?
    var publishedDate:Date?
    var source:String?
    var id:Int?
    var assetID:Int?
    var views:Int?
    var desFacet:[String]?
    var orgFacet:[String]?
    var perFacet:[String]?
    var geoFacet:[String]?
    var media:[Media]?
    var uri:String?
    required init() {
        
    }
    func sundeedAPIMapping(map: SundeedAPIMap) {
        self.url <- map["url"]
        self.adxKeywords <- map["adx-keywords"]
        self.column <- map["column"]
        self.section <- map["section"]
        self.byline <- map["byline"]
        self.type <- map["type"]
        self.title <- map["title"]
        self.abstract <- map["abstract"]
        self.publishedDate <- (map["published_date"],APIDateConverter())
        self.source <- map["source"]
        self.id <- map["id"]
        self.assetID <- map["asset_id"]
        self.views <- map["views"]
        self.desFacet <- map["des_facet"]
        self.orgFacet <- map["org_facet"]
        self.perFacet <- map["per_facet"]
        self.geoFacet <- map["geo_facet"]
        self.media <- map["media"]
        self.uri <- map["uri"]
    }
    
}
