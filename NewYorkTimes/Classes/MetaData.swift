//
//  MetaData.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import Foundation

class MetaData:SundeedAPIMappable{
    
    var url:String?
    var format:String?
    var height:Int?
    var width:Int?
    
    required init(){
        
    }
    func sundeedAPIMapping(map: SundeedAPIMap) {
        self.url <- map["url"]
        self.format <- map["format"]
        self.height <- map["height"]
        self.width <- map["width"]
    }
}
