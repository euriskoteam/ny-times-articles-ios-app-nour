//
//  ArticleViewController.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class ArticleViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var abstractLabel: UILabel!
    @IBOutlet weak var goToButton: UIButton!
    weak var article:Article!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.goToButton.setTitle(self.article.source, for: .normal)
        self.collectionView.register(UINib(nibName: "MediaCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: "reuseMedia")
        self.abstractLabel.text = article.abstract
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        self.title = self.article.title
        self.abstractLabel.numberOfLines = -1
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let collectionViewLayout = self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout{
            var size:CGFloat = self.collectionView.frame.width/2
            if self.collectionView.frame.width/2 > self.collectionView.frame.size.height - 16 {
                size = self.collectionView.frame.size.height - 16
            }
            collectionViewLayout.itemSize = CGSize(width: size, height: size)
            collectionViewLayout.scrollDirection = .horizontal
            
            
            if self.article.media != nil {
                let totalCellWidth = size * CGFloat(self.article.media!.count)
                let totalSpacingWidth = 4 * (self.article.media!.count - 1)
                
                let leftInset = (self.collectionView.frame.width - totalCellWidth + CGFloat(totalSpacingWidth)) / 2
                let rightInset = leftInset
                
                self.collectionView.contentInset = UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
            }
            
            
        }
    }
    @IBAction func goToButtonPressed(_ sender: Any) {
        guard let url = URL(string: self.article.url!) else {
            return
        }
        
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    deinit {
        print("deinit")
    }
}

extension ArticleViewController:UICollectionViewDataSource,UICollectionViewDelegate{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let media = self.article.media {
            return media.count
        }
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "reuseMedia", for: indexPath) as! MediaCollectionViewCell
        if let media = self.article.media {
                collectionViewCell.configureCell(withMedia: media[indexPath.row])
        }
        return collectionViewCell
    }
    
}
