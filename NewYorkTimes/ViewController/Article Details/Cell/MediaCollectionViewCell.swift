//
//  MediaCollectionViewCell.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class MediaCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var caption: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func configureCell(withMedia media:Media){
        self.caption.text = media.caption
        if let metaData = media.mediaMetadata{
            if metaData.count > 0 {
                Main.controller.getImage(url: metaData.last?.url) { (image) in
                    self.imageView.image = image
                }
            }
        }
    }
}
