//
//  ArticleTableViewCell.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articletitleLabel: UILabel!
    @IBOutlet weak var articleDescriptionLabel: UILabel!
    @IBOutlet weak var articleDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.articletitleLabel.numberOfLines = 2
        self.articleDescriptionLabel.numberOfLines = 2
        self.articleImageView.clipsToBounds = true
        self.articleImageView.layer.cornerRadius = 30
    }

    func configureCell(withArticle article:Article){
        
        self.articletitleLabel.text = article.title
        self.articleDescriptionLabel.text = article.abstract
        self.articleDateLabel.text = article.publishedDate?.representableString()
        self.articleImageView.image = nil
        if let media = article.media{
            if media.count > 0 {
                if let metaData = media.first!.mediaMetadata {
                    if metaData.count > 0 {
                        Main.controller.getImage(url: metaData.first!.url) { (image) in
                            self.articleImageView.image = image
                        }
                    }
                }
            }
        }
        
    }
    
}
