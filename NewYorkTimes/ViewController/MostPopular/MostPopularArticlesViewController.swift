//
//  MostPopularArticlesViewController.swift
//  NewYorkTimes
//
//  Created by Nour Sandid on 2/21/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import UIKit

class MostPopularArticlesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    private let refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.tableFooterView = UIView()
        self.tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: "reuseArticles")
        self.title = "NY_Times_Most_Popular".localized()
        self.tableView.backgroundView = self.activityIndicator
        self.activityIndicator.startAnimating()
        self.activityIndicator.hidesWhenStopped = true
        self.refreshControl.addTarget(self, action: #selector(self.getArticles), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.tableView.refreshControl = self.refreshControl
        } else {
            self.tableView.addSubview(self.refreshControl)
        }
        if traitCollection.forceTouchCapability == .available{
            self.registerForPreviewing(with: self, sourceView: self.tableView)
        }
        getArticles()
        
    }
    @objc func getArticles(){
        Main.controller.getMostViewedArticles(period: Period.month.rawValue) { (success, articles) in
            if success{
                self.tableView.reloadData()
            }
            else{
                let alertController = UIAlertController(title: "OOPS".localized(), message: "Somethings_Went_Wrong!".localized(), preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: nil))
                alertController.addAction(UIAlertAction(title: "Try_Again".localized(), style: .default, handler: { (a) in
                    self.getArticles()
                }))
                self.present(alertController, animated: true, completion: nil)
            }
            self.activityIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
        }
    }
}

extension MostPopularArticlesViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Main.controller.user.articles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "reuseArticles", for: indexPath) as! ArticleTableViewCell
        tableViewCell.configureCell(withArticle: Main.controller.user.articles[indexPath.row])
        return tableViewCell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
        let articleViewController = ArticleViewController(nibName:"ArticleViewController",bundle:Bundle.main)
        articleViewController.article = Main.controller.user.articles[indexPath.row]
        self.navigationController?.pushViewController(articleViewController, animated: true)
    }
}
extension MostPopularArticlesViewController:UIViewControllerPreviewingDelegate {
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
        self.navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
        let indexPath = self.tableView.indexPathForRow(at: location)
        if indexPath != nil && self.tableView.cellForRow(at: indexPath!) != nil{
            let articleViewController = ArticleViewController(nibName:"ArticleViewController",bundle:Bundle.main)
            articleViewController.article = Main.controller.user.articles[indexPath!.row]
            return articleViewController
        }
        else{
            return nil
        }
    }
}
