//
//  NewYorkTimesTests.swift
//  NewYorkTimesTests
//
//  Created by Nour Sandid on 2/23/19.
//  Copyright © 2019 NewYorkTimes. All rights reserved.
//

import XCTest
@testable import NewYorkTimes

class NewYorkTimesTests: XCTestCase {
    var metaJSONString:String!
    var mediaJSONString:String!
    var metaAPIRequest:SundeedAPIRequest<MetaData>!
    var mediaAPIRequest:SundeedAPIRequest<Media>!
    override func setUp() {
        metaJSONString = "{\"url\":\"https://static01.nyt.com/images/2019/02/18/opinion/18x-coldcase/18x-coldcase-thumbStandard.jpg\",\"format\":\"Standard Thumbnail\",\"height\":75,\"width\":75}"
        metaAPIRequest = SundeedAPIRequest<MetaData>()
        
        mediaJSONString = "{\"type\":\"image\",\"subtype\":\"photo\",\"caption\":\"Caption Test\",\"copyright\":\"copyright test\",\"approved_for_syndication\":0,\"media-metadata\":[{\"url\":\"https://static01.nyt.com/images/2019/02/14/us/british-irish-dialect-quiz-promo-1550183830563/british-irish-dialect-quiz-promo-1550183830563-thumbStandard-v3.jpg\",\"format\":\"Standard Thumbnail\",\"height\":75,\"width\":75},{\"url\":\"https://static01.nyt.com/images/2019/02/14/us/british-irish-dialect-quiz-promo-1550183830563/british-irish-dialect-quiz-promo-1550183830563-mediumThreeByTwo210-v3.jpg\",\"format\":\"mediumThreeByTwo210\",\"height\":140,\"width\":210},{\"url\":\"https://static01.nyt.com/images/2019/02/14/us/british-irish-dialect-quiz-promo-1550183830563/british-irish-dialect-quiz-promo-1550183830563-mediumThreeByTwo440-v3.jpg\",\"format\":\"mediumThreeByTwo440\",\"height\":293,\"width\":440}]}"
        mediaAPIRequest = SundeedAPIRequest<Media>()
    }

    override func tearDown() {
        metaAPIRequest = nil
        mediaAPIRequest = nil
    }

    func testConvertToDictionnary() {
        let dic = metaAPIRequest.convertToDictionary(data: metaJSONString.data(using: .ascii) ?? Data())
        XCTAssertNotNil(dic, "error creating map")
        
    }
    func testEmbedeObjectsConvert(){
        let dic = mediaAPIRequest.convertToDictionary(data: mediaJSONString.data(using: .ascii) ?? Data())
        XCTAssertNotNil(dic, "error creating map")
    }
    func testCreateMap(){
        let dic = metaAPIRequest.convertToDictionary(data: metaJSONString.data(using: .ascii) ?? Data())
        let map = SundeedAPIMap(dictionnary:dic!)
        XCTAssertNotNil(map, "error creating map")
    }
    func testCreateEmbedeObjectsMap(){
        let dic = mediaAPIRequest.convertToDictionary(data: mediaJSONString.data(using: .ascii) ?? Data())
        let map = SundeedAPIMap(dictionnary:dic!)
        XCTAssertNotNil(map, "error creating map")
    }
    func testMapping(){
        let dic = metaAPIRequest.convertToDictionary(data: metaJSONString.data(using: .ascii) ?? Data())
        let map = SundeedAPIMap(dictionnary:dic!)
        let metaData = MetaData()
        metaData.sundeedAPIMapping(map: map)
        XCTAssertNotNil(metaData, "error while parsing json")
        XCTAssertNotNil(metaData.format, "error while parsing json")
        XCTAssertNotNil(metaData.height, "error while parsing json")
        XCTAssertNotNil(metaData.width, "error while parsing json")
    }
    func testEmbedeObjectsMapping(){
        let dic = mediaAPIRequest.convertToDictionary(data: mediaJSONString.data(using: .ascii) ?? Data())
        let map = SundeedAPIMap(dictionnary:dic!)
        let media = Media()
        media.sundeedAPIMapping(map: map)
        XCTAssertNotNil(media, "error while parsing json")
        XCTAssertNotNil(media.approvedForSyndication, "error while parsing json")
        XCTAssertNotNil(media.caption, "error while parsing json")
        XCTAssertNotNil(media.copyright, "error while parsing json")
        XCTAssertNotNil(media.mediaMetadata, "error while parsing json")
        XCTAssertNotNil(media.subtype, "error while parsing json")
        XCTAssertNotNil(media.type, "error while parsing json")
    }
    
    func testPerformanceExample() {
        self.measure {
            let dic = metaAPIRequest.convertToDictionary(data: metaJSONString.data(using: .ascii) ?? Data())
            let map = SundeedAPIMap(dictionnary:dic!)
            let metaData = MetaData()
            metaData.sundeedAPIMapping(map: map)

        }
    }
    func testEmbededPerformanceExample() {
        self.measure {
            let dic = mediaAPIRequest.convertToDictionary(data: mediaJSONString.data(using: .ascii) ?? Data())
            let map = SundeedAPIMap(dictionnary:dic!)
            let media = Media()
            media.sundeedAPIMapping(map: map)
            XCTAssertNotNil(media, "error while parsing json")
            XCTAssertNotNil(media.approvedForSyndication, "error while parsing json")
            XCTAssertNotNil(media.caption, "error while parsing json")
            XCTAssertNotNil(media.copyright, "error while parsing json")
            XCTAssertNotNil(media.mediaMetadata, "error while parsing json")
            XCTAssertNotNil(media.subtype, "error while parsing json")
            XCTAssertNotNil(media.type, "error while parsing json")
        }
    }
}
